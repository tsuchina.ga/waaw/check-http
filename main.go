package main

import (
    "log"
    "github.com/BurntSushi/toml"
    "github.com/fsnotify/fsnotify"
    "sync"
    "time"
    "net/http"
    "net/smtp"
    "strings"
    "net/url"
    "io/ioutil"
    "bytes"
)

const SettingFile = "setting.toml"
var Setting Settings

func main() {
    log.SetFlags(log.LstdFlags | log.Lshortfile)

    // 設定ファイルを最初に読んどく
    getSetting()

    // ファイル変更検知
    watcher, err := fsnotify.NewWatcher()
    if err != nil {
        log.Fatalln(err)
    }
    defer watcher.Close()

    go func() {
        for {
            select {
            case event := <- watcher.Events:
                // 書き込みがあった場合に設定ファイルの読み直しを行なう
                if event.Op & fsnotify.Write == fsnotify.Write {
                    getSetting()
                }
            case err := <- watcher.Errors:
                log.Fatalln(err)
            }
        }
    }()

    if err = watcher.Add(SettingFile); err != nil {
        log.Fatalln(err)
    }

    // 監視を開始する！
    go check()

    // I'm alive!
    go alive()

    // 待ち続ける
    wg := new(sync.WaitGroup)
    wg.Add(1)
    wg.Wait()
}

func getSetting() {
    if _, err := toml.DecodeFile(SettingFile, &Setting); err != nil {
        log.Fatalln(err)
    }
    log.Println(Setting)
}

func check() {
    for {
        go func() {
            for _, target := range Setting.Targets {
                go func(target Target) {
                    client := &http.Client{Timeout: time.Duration(5) * time.Second}
                    req, _ := http.NewRequest("GET", target.Page, nil)
                    if resp, err := client.Do(req); err != nil {
                        // エラー送信
                        log.Println(err)
                        send(target, "error", err.Error())
                    } else if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
                        // エラー送信
                        log.Println(target.Page, resp.Status)
                        send(target, "error", err.Error())
                    } else {
                        // 上記以外は正常
                        log.Println(target.Page, "OK")
                    }
                }(target)
            }
        }()

        time.Sleep(time.Duration(Setting.Interval.Check) * time.Second)
    }
}

func alive() {
    time.Sleep(5 * time.Second) // 初回のcheckと被らないようにするため

    for {
        target := Target{
            Page: "alive",
            Send: "chatwork",
        }
        go send(target, "alive", "I'm alive!!")
        time.Sleep(time.Duration(Setting.Interval.Alive) * time.Second)
    }
}

func send(target Target, subject, message string) {
    switch target.Send {
    case "email":
        sendMail(target, subject, message)
    case "chatwork":
        sendChatwork(target, subject, message)
    case "both":
        sendMail(target, subject, message)
        sendChatwork(target, subject, message)
    }
}

func sendMail(target Target, subject, message string) (err error) {
    host := Setting.Mail.Host
    port := Setting.Mail.Port
    user := Setting.Mail.User
    password := Setting.Mail.Password
    from := Setting.Mail.From
    to := Setting.Mail.To

    body := "To: " + to + "\r\n" +
        "Subject: check-http get " + subject + "!!\r\n" +
        "\r\n" + target.Page + "\r\n" + time.Now().String() + "\r\n" + message + "\r\n"

    auth := smtp.PlainAuth("", user, password, host)
    return smtp.SendMail(host + ":" + port, auth, from, []string{to}, []byte(body))
}

func sendChatwork(target Target, subject, message string) {

    body := "[info][title]check-http get " + subject + "!![/title]" +
        "\r\n" + target.Page + "\r\n" + time.Now().String() + "\r\n" + message + "[/info]\r\n"
    values := url.Values{}
    values.Add("body", body)

    client := &http.Client{Timeout: time.Duration(5) * time.Second}
    req, _ := http.NewRequest("POST",
        "https://api.chatwork.com/v2/rooms/" + Setting.Chatwork.RoomId + "/messages",
        bytes.NewBufferString(values.Encode()))

    req.Header.Add("X-ChatWorkToken", Setting.Chatwork.Token)
    req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

    if resp, err := client.Do(req); err != nil {
        log.Fatalln(err)
    } else {
        defer resp.Body.Close()
        bodyBytes, _ := ioutil.ReadAll(resp.Body)
        bodyString := string(bodyBytes)
        log.Println(resp.Status, bodyString, strings.NewReader(values.Encode()))
    }
}

type Settings struct {
    Interval Interval `toml:"interval"`
    Mail Mail `toml:"mail"`
    Chatwork Chatwork `toml:"chatwork"`
    Targets []Target `toml:"target"`
}

type Interval struct {
    Check int `toml:"check"`
    Alive int `toml:"alive"`
}

type Mail struct {
    Host string `toml:"host"`
    Port string `toml:"port"`
    User string `toml:"user"`
    Password string `toml:"pass"`
    From string `toml:"from"`
    To string `toml:"to"`
}

type Chatwork struct {
    Token string `toml:"token"`
    RoomId string `toml:"room_id"`
}

type Target struct {
    Page string `toml:"page"`
    Send string `toml:"send"`
}
