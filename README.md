# _WAAW08_ check-http

毎週1つWebアプリを作ろうの8回目。

期間は18/06/29 - 18/07/05

WEBページを登録し、200番が返ってくることを監視。問題があれば任意の方法で通知(email/chatwork)


## ゴール

* [x] WEBページを監視できる
* [ ] Redirectの場合は辿れる
* [x] emailで通知できる
* [x] chatworkで通知できる


## 目的

* [x] 提供するサービスが落ちていないかを検知


## 課題

* [x] 設定ファイル

    * [x] [【個人メモ】設定ファイルフォーマットにはTOMLがいいのかも - Qiita](https://qiita.com/futoase/items/fd697a708fcbcee104de)
    * [x] [設定ファイル記述言語 TOML - Qiita](https://qiita.com/b4b4r07/items/77c327742fc2256d6cbe)

* [x] 設定ファイルの変更検知

    * [x] [fsnotify/example_test.go at master · fsnotify/fsnotify](https://github.com/fsnotify/fsnotify/blob/master/example_test.go)

* [x] intellijでtoml対応と拡張子紐づけ
    * [x] [Toml :: JetBrains Plugin Repository](https://plugins.jetbrains.com/plugin/8195-toml)
    * [x] [IntelliJ IDEAに任意の拡張子を登録する - Qiita](https://qiita.com/EichiSanden/items/434665d3420c612ebe18)

* [ ] x509: certificate signed by unknown authority
    
    docker上から実行すると証明書がないからかエラーになる。オレオレ証明書を自動で作ればいけるけど、それよりかはchatworkとかのapi叩いたほうが早そう

* [x] chatwork api

    * [x] [チャットワークAPIドキュメント](http://developer.chatwork.com/ja/endpoint_rooms.html#POST-rooms-room_id-messages)
    * [x] [Go言語 http POSTする際のContent-Type - Qiita](https://qiita.com/yyoshiki41/items/fc878494e19b9de93d56)

## 振り返り

0. Idea:

    アイデアやテーマについて
    * 別件で必要だったものを詰め込んだのが発端
    * ファイル監視、サーバ監視、そしてその通知と保守時に必要な機能だと思う

0. What went right:

    成功したこと・できたこと
    * ファイルの変更監視
    * WEBサーバの生存監視
    * それらの通知

0. What went wrong:

    失敗したこと・できなかったこと
    * docker上でのメール送信(認証・証明書の問題)
    * WEBページでリダイレクトさせられる場合の生存監視
    * ファイルを複製して上書きされるようなタイプの更新を検知できない？

0. What I learned:

    学べたこと
    * 設定ファイルの監視というか、go言語のファイルとディレクトリの仕様
    * 動き続けるシステムを作るときのコツの初歩的な部分
    * やはりメール送信はネックになるので、代替手段としてchatworkやslackを用いること
    * 小さなことだけど、intellijでの辞書追加の有用性(笑
    

## サンプル

なし
